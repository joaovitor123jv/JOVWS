#######################################################
#Arquivo para configuração de rotas do método post
#######################################################

post '/cadastrar' do
	atualizaPilhaDePaginas '/cadastrar'
	if session[:logado] then
		redirect "erros/internal"
	else
		if params[:password] == params[:passwordConfirm]  then
			# Mandar pro banco
			DatabaseController.new().sendData(:usuario, 
									{
										nome: params[:nome], 
										email: params[:email], 
										senha: params[:password]
									})
			return "Dados atualizados <a href=\"#{paginaAnterior}\">Voltar</a>"
		else
			session[:mensagem] = "As senhas não correspondem, tente novamente"
			redirect '/cadastrar'
		end
	end
end

post '/enviarReclamacao' do
	puts "Message: #{params[:message]} \n\t\t\t Email:#{params[:email]}"
	if not session[:logado] then
		puts "Usuário não conectado detectado"
		session[:ERRO] = "Você precisa estar logado para continuar essa operação"
		puts "Dados de erro de sessão atualizados, redirecionando para #{paginaAtual}"
		redirect paginaAtual
		puts "Redirecionado"
	else
		puts "Função incompleta detectada"
		return "Esta função ainda está inoperante"
	end
end

# PARA JSON, SOMENTE
#		request.body.rewind
#		data = JSON.parse request.body.read
#		puts "LSKDJSAKDHASJKHDASKLHDKASLHDJKASHDJKASLHDJKLSAHJDKLSAHJDKLSAHJDKLHSAJKDLHSJAKD"
#		puts "Dados recebidos = #{data}"
#		puts "LSKDJSAKDHASJKHDASKLHDKASLHDJKASHDJKASLHDJKLSAHJDKLSAHJDKLSAHJDKLHSAJKDLHSJAKD"
#		erb :"paginas/login"
