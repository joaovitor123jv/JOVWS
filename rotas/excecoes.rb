get "/nao-encontrado" do
	status 404
	erb :"erro/404"
end

not_found do
	status 404
	erb :"erro/404"
end

#Renderiza página que informa tentativa de acesso a página não autorizada
get "/nao-autorizado" do
	status 403
	erb :"erro/nao-autorizado"
end


get "/erros-internal" do
	status 500
	erb :"erro/interno"
end

error 500 do
	status 500
	erb :"erro/interno"
end