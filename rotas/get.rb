
###################################
#As rotas de GET serão colocadas aqui
###################################

######## PUBLICO ###################

get '/' do
	atualizaPilhaDePaginas '/'
	session[:web] = true
	erb :index
end

get '/index' do
	session[:web] = true
	atualizaPilhaDePaginas '/'
	erb :index
end

get '/login' do
	session[:web] = true
	atualizaPilhaDePaginas '/login'
	if session[:logado] and session[:contratante] then
		redirect "/adm"
	else
		erb :"paginas/login"
	end
end

get '/logout' do
	atualizaPilhaDePaginas '/logout'
	session[:web] = true
	session[:logado] = false
	session[:contratante] = false
	redirect "/"
end


######## ADM ################### |
################################ V

get "/visualizar/pacote" do
	if not session[:pacote]
		redirect "/erros-internal"
	elsif not session[:logado]
		redirect "/login"
	elsif not session[:contratante]
		redirect "/nao-autorizado"
	end
	erb :"paginas/adm/visualizarPacote"
end

######################			|
########################  ROOT 	V


get '/rootPage' do
	session[:web] = true
	if not session[:logado] then
		redirect paginaAtual
	else
		if session[:usuarioRoot] then
			erb :"ROOT/index"
		else
			redirect "/nao-autorizado"
		end
	end
end

get '/ROOT/cadastrarPacote' do
	session[:web] = true
	if session[:logado] and session[:usuarioRoot] then
		erb :"ROOT/funcoesDeTeste/cadastrarPacote"
	else
		redirect "/nao-autorizado"
	end
end
