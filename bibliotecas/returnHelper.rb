# Biblioteca com funções de retorno para a API

def retornaNaoAutorizado
	status 403
	return "ERRO"
end

def retornaOK(resposta = "OK")
	status 200
	return resposta
end

def retornaBSONComoJSON(bson = "{\"erro\":\"ARQUIVO VAZIO\"}")
	status 200
	stream do |out|
		bson.each do |doc|
			out<< JSON.pretty_generate(doc)
		end
	end
end

def retornaErro(erro = "ERRO")
	status 500
	return erro
end