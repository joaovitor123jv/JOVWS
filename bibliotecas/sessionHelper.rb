
def atualizaPilhaDePaginas(pagina)
	if session[:ultimaPaginaAcessada] != pagina then
		session[:penultimaPaginaAcessada] = session[:ultimaPaginaAcessada]
		session[:ultimaPaginaAcessada] = pagina
	end
end

def paginaAnterior
	session[:penultimaPaginaAcessada] ? 
		session[:penultimaPaginaAcessada] :
		"/";
end

def paginaAtual
	session[:ultimaPaginaAcessada]
end