require 'net/http'
require 'uri'
require 'json'


uri = URI.parse("http://localhost:8000/login")
header = {'Content-Type': 'text/json'}

puts "Ok, vamos cadastrar um pacote nor servidor... \nDigite o e-mail de root (root@root, provavelmente)"
rootLogin = gets.chomp
rootPassword = nil

if rootLogin.empty? then
	rootLogin = "root@root"
	rootPassword = "root"
else 
	puts "\n\nDigite a senha do root (provavelmente \"root\") "
	rootPassword = gets.chomp
end


data = {
			usuario: 	
			{
				email: rootLogin,
				senha: rootPassword
			}
		}

puts "\n\nOK... Os dados foram recebidos e configurados"
puts "Conectando-se ao servidor"
	
# Create the HTTP objects
http = Net::HTTP.new(uri.host, uri.port)
puts "Gerando requisição post para página especificada"
request = Net::HTTP::Post.new(uri.request_uri, header)
puts "Convertendo dados informados para JSON"
request.body = data.to_json

puts "Dados convertidos\n\n #{data.to_json} \n\n"

puts "Enviando requisição (dados) para o servidor"
# Send the request
response = http.request(request)

puts "Dados enviados. \nMostrando resposta recebida :\n\n"
puts response
