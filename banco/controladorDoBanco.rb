require 'mongo'

class DatabaseController
	def initialize
		if BancoLocal then
			@client = Mongo::Client.new(["127.0.0.1:#{DatabasePort}"], :database => DatabaseName)
		else
			@client = Mongo::Client.new(DatabaseIpAddress)
		end
		@database = @client.database
	end

	def getDatabaseCollections
		@database.collection_names
	end

	def getClient
		@client
	end

	def selectCollection(name)
		@client[name]
	end

	def sendData(collection, document)
		@client[collection].insert_one(document)
	end

	def findFirst(collection, document)
		@client[collection].find(document).first()
	end

	def findOne(collection, document, returnFormat = nil)
		if returnFormat == nil then
			@client[collection].find(document).limit(1).first()
		else
			puts "Colocando formato de retorno na operação do banco de dados" #Não funciona !!!
			@client[collection].find(document).projection(returnFormat).limit(1).first()
		end
	end

	def findLast(collection, document)
		@client[collection].find(document).sort(:_id => -1).first()
	end

	def find(collection, document)
		@client[collection].find(document)
	end

	def findAll(collection, document)
		resultado = Array.new
		i = 0
		retorno = @client[collection].find(document)
		retorno.each do |doc|
			resultado[i] = doc
			i += 1
		end
		return resultado
	end
	
	def increaseValueOnLastDocument(collection, document, data)
		id = findLast(collection, document)["_id"]
		@client[collection].update_one({_id: id}, "$inc" => data)

	end

	def updateElements(collection, document, data)
		@client[collection].find(document).update_many('$set'=> data)
	end

	def updateElement(collection, document, data)
		@client[collection].find(document).update_one('$set'=> data)
	end

	def addElementToArray(collection, document, data)
		@client[collection].find(document).update_one({"$push" => data}, {upsert: true})
	end

	def replaceElement(collection, document, data)
		@client[collection].find(document).replace_one(data)
	end

	def deleteAll(collection, document)
		@client[collection].find(document).delete_many
	end

	def deleteOne(collection, document)
		@client[collection].find(document).delete_one
	end
end
