########################################################
#As configurações do banco de dados estão aqui
########################################################

RootName = "Usuário Root"
RootLogin = "root@root"
RootPassword = "root"

#BancoLocal = false
BancoLocal = true

DatabasePort = "27017"

if BancoLocal then
	DatabaseIpAddress = "127.0.0.1"
	DatabaseName = "JOVWS DB"

	if Mongo::Client.new(["#{DatabaseIpAddress}:#{DatabasePort}"], :database => DatabaseName)[:usuario].find({email: RootLogin, senha: RootPassword}).first() == nil then
		puts "Não foi identificado pelo ou menos uma entrada para root que satisfaça as opções desejadas"
		Mongo::Client.new(["#{DatabaseIpAddress}:#{DatabasePort}"], :database => DatabaseName)[:usuario].insert_one(
		{
			nome: RootName,
			email: RootLogin,
			senha: RootPassword
		})
	else
		puts "Usuário ROOT já está cadastrado no banco de dados, nada a ser feito"
	end


else
	AtlasUser = "atlasUsername@host.com"
	AtlasPassword = "atlasPassword"
	DatabaseLogin = "atlasDatabaseUserName"
	DatabasePassword = "atlasDatabaseUserPassword"
	AtlasDatabaseName = "atlasDatabaseName(host)"
#mongodb+srv://testes-banco-coisa.mongodb.net/test   → Conexao via shell
## MongoDB URI must be in the following format: mongodb+srv://[username:password@]host[/[database][?options]]

	DatabaseIpAddress = "mongodb+srv://#{DatabaseLogin}:#{DatabasePassword}@#{AtlasDatabaseName}/test"
	DatabaseName = "admin"

	puts "Inicializando primeira conexão com banco de dados"

	if Mongo::Client.new(DatabaseIpAddress)[:usuario].find({email: RootLogin, senha: RootPassword}).first() == nil then
		puts "Não foi identificado pelo ou menos uma entrada para root que satisfaça as opções desejadas"
		Mongo::Client.new(DatabaseIpAddress)[:usuario].insert_one(
		{
			nome: RootName,
			email: RootLogin,
			senha: RootPassword
		})
	else
		puts "Usuário ROOT já está cadastrado no banco de dados, nada a ser feito"
	end

end



