################################################################
#As configurações do servidor serão colocadas aqui (ip, porta...)
################################################################


set :port, 8000

set :public_folder, Proc.new{File.join(root, "publico/assets")}
set :views, Proc.new{File.join(root, "publico/views")}

enable :sessions

